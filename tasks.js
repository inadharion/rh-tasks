
(jQuery)(function($){
	$('input[type=checkbox]').on('change', function(){
		var id = $(this).data('id');
		var checked = this.checked;
		var element = $(this);

		jQuery.post(
			'admin-ajax.php', 
			{
				'action': 'tasks_toggle_done',
				'id': id,
				'checked': checked
			}, 
			function(e,a) {
				if(a=='success') {
					$(element).css('border-color', 'rgba(100, 200, 100, 1)');
					window.location.reload();
				}
			}
		);
	});
});