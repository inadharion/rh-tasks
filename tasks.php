<?php
/*
Plugin Name: Tasks
Author: Ravn Heggerud
Version: 0.1
 */

function tasks_init() {
	register_post_type('task',
		array(
			'labels' => array(
				'name' => __('Tasks'),
				'singular_name' => __('Task'),
			),
			'public' => true,
			'show_in_menu' => true,
			'show_ui' => true,
			'supports' => array('title'), //, 'custom-fields'),
			'register_meta_box_cb' => 'tasks_metabox',
		)
	);

	register_meta('task', 'task_status', array(
		'show_in_rest' => true,
		'single' => true,
		'type' => 'boolean',
	));
	add_action('admin_enqueue_scripts', 'enqueue_tasks_script');
}

add_action('init', 'tasks_init');

function enqueue_tasks_script() {
	wp_enqueue_script('tasks', plugin_dir_url(__FILE__) . 'tasks.js', array(), '1.0');
}

function tasks_metabox() {
	add_meta_box(
		'tasks_metabox',
		'Tasks',
		'tasks_metabox_html',
		'task',
		'side'
	);
}
function tasks_metabox_html() {
	print 'Total: ' . tasks_count_total() . '<br />';
	print 'Unfinished: ' . (tasks_count_total() - tasks_count_done()) . '<br />';
}

function tasks_menu($e) {
	global $menu;
	$count = tasks_count_total() - tasks_count_done();

	if ($count == 0) {
		return;
	}

	foreach ($menu as $index => $entry) {
		if ($entry[0] == 'Tasks') {
			$menu[$index][0] = 'Tasks <span class="awaiting-mod">' . $count . '</span>';
		}

	}
}
add_action('admin_menu', 'tasks_menu');

add_filter('manage_task_posts_columns', 'hs_task_table_head');
function hs_task_table_head($columns) {
	$columns['task_status'] = 'Done';
	return $columns;

}
add_action('manage_task_posts_custom_column', 'tasks_table_content', 10, 2);

function tasks_table_content($column_name, $post_id) {
	if ($column_name == 'task_status') {
		$done = get_post_meta($post_id, 'task_status', true);
		echo '<input type="checkbox" value="1" data-id="' . $post_id . '"' . ($done == 1 ? ' checked' : '') . ' />';
	}
}

function tasks_notify() {
	$class = 'notice notice-warning';
	$count = tasks_count_total() - tasks_count_done();
	$message = __('You have ' . ($count) . ' unfinished <a href="edit.php?post_type=task">tasks</a>');

	if ($count > 0) {
		printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), $message);
	}

}
add_action('admin_notices', 'tasks_notify');

function tasks_count_done() {
	$args = array(
		'post_type' => 'task',
		'post_status' => 'publish',
		'fields' => 'ids',
		'meta_query' => array(
			array(
				'key' => 'task_status',
				'value' => '1',
			),
		),
	);
	$results = new WP_Query($args);
	return count($results->posts);
}
function tasks_count_total() {
	$args = array(
		'post_type' => 'task',
		'post_status' => 'publish',
		'fields' => 'ids',
	);
	$results = new WP_Query($args);
	return count($results->posts);
}

add_action('wp_ajax_tasks_toggle_done', 'tasks_toggle_done');

function tasks_toggle_done() {
	update_post_meta($_POST['id'], 'task_status', ($_POST['checked'] == 'true' ? 1 : 0));
	wp_die();
}
